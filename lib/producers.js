const nodejs = true

function put ({ hostname, port, path, body }) {
    if(nodejs) {
        const http = require('http');
    
        const data = new TextEncoder().encode(
            JSON.stringify(body)
        )
        
        const options = {
            hostname,
            port,
            path,
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length
            }
        }
        
        return new Promise(function(resolve, reject){
            const req = http.request(options, res => {
            console.log(`statusCode: ${res.statusCode}`)
            
                res.on('data', d => {
                    process.stdout.write(d)
                })

                res.on('end', () => {
                    resolve()
                });
            })
            
            req.on('error', error => {
                console.error(error)
                reject(error)
            })
            
            req.write(data)
            req.end()
        })
    } else {
        return fetch(`${hostname}:${port}${path}`, {
            body,
            method: 'PUT'
        })
    }

}

function addProducer (producerId) {
    return put({
        hostname: 'localhost',
        port: 5027,
        path: `/v1/producers/${producerId}`,
        body: {
            name: 'beats test producer',
            areaType: 'WASHING',
            id: '121212'
        }
    })
}

function addMessage (producerId) {
    const messageId = 0
    return put({
        hostname: 'localhost',
        port: 5027,
        path: `/v1/producers/${producerId}/messages/${messageId}`,
        body: {
            translations: {
                bla: {
                    text: 'bla translation text'
                }
            },
        }
    })
}

function addProgram (producerId) {
    const programId = 0
    return put({
        hostname: 'localhost',
        port: 5027,
        path: `/v1/producers/${producerId}/programs/${programId}`,
        body: {
            name: 'beats bla program'
        }
    })
}

function addSubProducer (producerId) {
    const subProducerId = 0
    return put({
        hostname: 'localhost',
        port: 5027,
        path: `/v1/producers/${producerId}/sub-producers/${subProducerId}`,
        body: {
            isOutput: true
        }
    })
}

const addData = async () => {
    const id = "121212"
    await addProducer(id)
    await addMessage(id)
    await addProgram(id)
    await addSubProducer(id)
}

try {
    addData()
} catch(e) {
    console.error(e)
}


// fetch(`http://devldevslx02/api/locations/v1/producers/${_.id}`, {
//   "method": "DELETE"
// });

fetch(`http://devldevslx02/api/locations/v1/producer-views/${_.id}`, {
    "method": "DELETE"
})

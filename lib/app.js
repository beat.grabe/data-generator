const Axios = require('axios')
const Chance = require('chance')
const Color = require('color')
const Promise = require('bluebird')

const chance = new Chance()
const colorFormat = 'hex'

const API_HOSTS = {
  docker: 'http://devlslx02.kg.local',
  remote: 'http://10.8.15.116'
}

const api = Axios.create({
  baseURL: `${API_HOSTS.docker}/api/master-data/v1`,
  timeout: 30000
})

function moep() {
  api.get('customers')
    .then(response => response.data)
    .then(customers => {
      const ids = customers.map(customer => {
        // const name = chance.company()
        // customer.name = name
        // customer.shortName = name

        api.put(`customers/${customer.id}`, changeColor(customer))
      })
    })
    .catch(error => {
      console.error(error)
    })
}

const dataTypes = ['articles', 'categories', 'customers', 'employees']

function anonymizeEmployees () {
  return dm.get('/employees')
    .then(response => response.data)
    .then(employees => Promise.map(employees, employee => {
      employee.firstName = chance.first()
      employee.lastName = chance.last()
      employee.backgroundColorHtml = chance.color({format: colorFormat})
      employee.foregroundColorHtml = chance.color({format: colorFormat})

      return dm.put(`/employees/${employee.id}`, employee)
    }, {
      concurrency: 1
    }))
    .catch(error => {
      console.error(error)
    })
}

const CSS_LEVEL_1_COLORS = [
  'black',
  'silver',
  'gray',
  'white',
  'maroon',
  'red',
  'purple',
  'fuchsia',
  'green',
  'lime',
  'olive',
  'yellow',
  'navy',
  'blue',
  'teal',
  'aqua'
]

const MATERIAL_COLORS = [
  '#F44336',
  '#e91e63',
  '#9c27b0',
  '#673ab7',
  '#3f51b5',
  '#2196F3',
  '#03a9f4',
  '#00bcd4',
  '#009688',
  '#4CAF50',
  '#8bc34a',
  '#cddc39',
  '#ffeb3b',
  '#ffc107',
  '#ff9800',
  '#ff5722',
  '#795548',
  '#607d8b',
  '#9e9e9e'
]

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
const changeColor = record => {
  const index = parseInt(record.id / 100) % MATERIAL_COLORS.length
  const colorName = MATERIAL_COLORS[index]

  const backgroundColor = Color(colorName)
  record.backgroundColorHtml = backgroundColor.hex()
  const foregroundColor = backgroundColor.isLight() ? Color('black') : Color('white')
  record.foregroundColorHtml = foregroundColor.hex()

  return record
}
const changeName = record => {
  const index = record.name.indexOf('COG')
  const name = record.name
  const newName = name.slice(0, index).trim()
  record.name = newName

  return record
}

function harmonizeAvatars (type) {
  if (!dataTypes.some(dataType => dataType === type)) return

  return dm.get(`/${type}`)
    .then(response => response.data)
    .then(data => {
      return data.reduce((promise, record) => {
        return promise.then(() => {
          record = changeColor(record)
          const request = dm.put(`/${type}/${record.id}`, record)
            .then(() => (console.log(`Record with ID ${record.id} was updated…`)))
            .catch(error => {
              console.error(error)
            })
          return Promise.all([delay(300), request])
        })
      }, Promise.resolve())
    })
    .then(() => console.info('Finish'))
    .catch(error => {
      console.error(error)
    })
}

// harmonizeAvatars('articles')
// harmonizeAvatars('categories')
// harmonizeAvatars('customers')
// harmonizeAvatars('employees')

function harmonizeNames (type) {
  if (!dataTypes.some(dataType => dataType === type)) return

  return dm.get(`/${type}`)
    .then(response => response.data)
    .then(data => {
      return data.reduce((promise, record) => {
        if (!record.name.includes('COG')) return Promise.resolve()
        return promise.then(() => {
          record = changeName(record)
          const request = dm.put(`/${type}/${record.id}`, record)
            .then(() => (console.log(`Record with ID ${record.id} was updated…`)))
            .catch(error => (console.error(error)))
          return Promise.all([delay(300), request])
        })
      }, Promise.resolve())
    })
    .catch(error => {
      console.error(error)
    })
}

// harmonizeNames('articles')
